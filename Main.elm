-- Attempt to implement Ranked pairs algorithm in Elm: https://en.wikipedia.org/wiki/Ranked_pairs


module Main exposing (..)

import Html exposing (Html, button, div, text, ol, li, h1)
import Html.Events exposing (onClick)
import Array
import Dict
import Random.List
import Random
import Debug


main =
    Html.program
        { init =
            init
                [ "Dark Souls"
                , "Journey"
                , "Mass Effect"
                , "Silent Hill 2"
                , "BioShock"
                , "World of Warcraft"
                , "Shadow of the Colossus"
                , "Battlefield 1942"
                , "Rock Band"
                , "Star Wars: Knights of the Old Republic"
                , "The Legend of Zelda"
                , "Red Dead Redemption"
                , "Final Fantasy VI"
                , "Castlevania: Symphony of the Night"
                , "Half-Life"
                , "Metal Gear Solid 3: Snake Eater"
                , "Sid Meier's Civilization IV"
                , "Star Wars: TIE Fighter"
                , "Halo: Combat Evolved"
                , "Super Mario Galaxy"
                , "Street Fighter II"
                , "Deus Ex"
                , "Baldur's Gate 2"
                , "Portal"
                , "Grand Theft Auto 5"
                , "Minecraft"
                , "Super Mario World"
                , "Chrono Trigger"
                , "Sid Meier's Pirates!"
                , "Super Mario 64"
                , "Tetris"
                , "Halo 2"
                , "The Legend of Zelda: Ocarina of Time"
                , "Super Metroid"
                , "Half-Life 2"
                , "Portal 2"
                , "Super Mario Bros."
                , "Doom"
                , "The Legend of Zelda: A Link To The Past"
                , "Super Mario Bros. 3"
                ]
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Candidate =
    String


type alias Pair =
    { candidateA : Candidate
    , candidateB : Candidate
    }


type alias Vote =
    { candidateA : Candidate
    , candidateB : Candidate
    , winner : Maybe Candidate
    }


type alias Model =
    { candidates : List Candidate
    , pairs : List Pair
    , currentVote : Maybe Pair
    , topList : List Candidate
    }


init : List String -> ( Model, Cmd Msg )
init candidates =
    let
        allPairs =
            createPairs candidates

        randomizedPairs =
            allPairs
                |> Random.List.shuffle
    in
        ( Model candidates allPairs Nothing [], (Random.generate NewShuffledPairs randomizedPairs) )



-- UPDATE


type Msg
    = AddVote Pair (Maybe Candidate)
    | DropCandidate Candidate
    | NewShuffledPairs (List Pair)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddVote currentVote candidate ->
            ( (vote model currentVote candidate), Cmd.none )

        NewShuffledPairs shuffledPairs ->
            ( { model
                | pairs = List.drop 1 shuffledPairs
                , currentVote = List.head shuffledPairs
              }
            , Cmd.none
            )

        DropCandidate candidate ->
            ( (dropCandidate model candidate), Cmd.none )


vote : Model -> Pair -> Maybe Candidate -> Model
vote model currentVote winningCandidate =
    let
        losingCandidate =
            case winningCandidate of
                Just winningCandidate ->
                    if currentVote.candidateA == winningCandidate then
                        Just currentVote.candidateB
                    else
                        Just currentVote.candidateA

                Nothing ->
                    Nothing

        newTopList =
            insertCandidateAboveCandidate model.topList winningCandidate losingCandidate

        candidatesToDrop =
            getCandidatesBelowCandidate newTopList losingCandidate

        newPairs =
            dropPairsWithCandidateAndCandidates model.pairs winningCandidate candidatesToDrop
    in
        { model
            | currentVote = List.head newPairs
            , pairs = List.drop 1 newPairs
            , topList = newTopList
        }


dropCandidate : Model -> Candidate -> Model
dropCandidate model candidate =
    let
        pairsWithoutCandidate =
            List.filter (\p -> p.candidateA /= candidate && p.candidateB /= candidate) model.pairs
    in
        { model
            | currentVote = List.head pairsWithoutCandidate
            , pairs = List.drop 1 pairsWithoutCandidate
            , topList = List.filter (\c -> c /= candidate) model.topList
        }


insertCandidateAboveCandidate : List Candidate -> Maybe Candidate -> Maybe Candidate -> List Candidate
insertCandidateAboveCandidate topList newCandidate oldCandidate =
    case ( newCandidate, oldCandidate ) of
        ( Just newCandidate, Just oldCandidate ) ->
            case topList of
                [] ->
                    [ newCandidate, oldCandidate ]

                x :: xs ->
                    -- If we reach our newCandidate first we just want to return our list
                    if x == newCandidate then
                        if List.member oldCandidate xs then
                            newCandidate :: xs
                        else
                            newCandidate :: xs ++ [ oldCandidate ]
                    else if x == oldCandidate then
                        newCandidate :: oldCandidate :: (List.filter (\c -> c /= newCandidate) xs)
                    else
                        x :: insertCandidateAboveCandidate xs (Just newCandidate) (Just oldCandidate)

        ( _, _ ) ->
            topList


getCandidatesBelowCandidate : List Candidate -> Maybe Candidate -> List Candidate
getCandidatesBelowCandidate topList candidate =
    case candidate of
        Just candidate ->
            case topList of
                [] ->
                    []

                x :: xs ->
                    if x == candidate then
                        xs
                    else
                        getCandidatesBelowCandidate xs (Just candidate)

        Nothing ->
            []


dropPairsWithCandidateAndCandidates : List Pair -> Maybe Candidate -> List Candidate -> List Pair
dropPairsWithCandidateAndCandidates pairs candidate candidates =
    case candidate of
        Just candidate ->
            List.filter (\p -> not (isCandidateOrCandidatesInPair p candidate candidates)) pairs

        Nothing ->
            pairs


isCandidateOrCandidatesInPair : Pair -> Candidate -> List Candidate -> Bool
isCandidateOrCandidatesInPair pair candidate candidates =
    (pair.candidateA == candidate && List.member pair.candidateB candidates) || (pair.candidateB == candidate && List.member pair.candidateA candidates)



-- VIEW


view : Model -> Html Msg
view model =
    case model.currentVote of
        Just currentVote ->
            div []
                [ h1 [] [ text "What's your favourite game?" ]
                , text (toString (1 + List.length model.pairs) ++ " more to go.")
                , renderVoteAlternative currentVote currentVote.candidateA
                , renderVoteAlternative currentVote currentVote.candidateB
                , button [ onClick (AddVote currentVote Nothing) ] [ text "Can't decide" ]
                ]

        Nothing ->
            div []
                [ text "You pushed through. Great job!"
                , div [] [ h1 [] [ text "Your top  all-time favourite games:" ], renderResultList model.topList ]
                ]


renderVoteAlternative currentVote candidate =
    div []
        [ button [ onClick (AddVote currentVote (Just candidate)) ] [ text candidate ]
        , button [ onClick (DropCandidate candidate) ] [ text "I haven't played this game" ]
        ]


renderResultList : List Candidate -> Html Msg
renderResultList resultList =
    ol []
        (List.map (\l -> li [] [ text l ]) resultList)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- Votes


createPairs : List Candidate -> List Pair
createPairs candidates =
    let
        candidatesWithIndexes =
            Array.indexedMap (\i c -> ( i, c )) (Array.fromList candidates)
    in
        Array.foldl (\( i, candidateA ) pairs -> List.append pairs (List.map (\candidateB -> Pair candidateA candidateB) (List.drop (i + 1) candidates))) [] candidatesWithIndexes
